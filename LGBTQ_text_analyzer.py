import requests
import os
import sys
import json
# pprint is pretty print (formats the JSON)
from pprint import pprint
from IPython.display import HTML
import math
import time 

#global variables
_debug = False
_microsoft_subscription_key = ""
_microsoft_api_url = ""

_pixel_lab_api_url = ""
_pixel_lab_api_token = ""
_seconds_in_between_unanalyzed_messages = ""
_page_size = 0

# loadConfiguration loads global variables from a json file

def loadConfiguration():

	global _microsoft_api_url, _microsoft_subscription_key, _pixel_lab_api_url, _pixel_lab_api_token,\
			_seconds_in_between_unanalyzed_messages, _page_size

	try:
        
		current_file_dir =  os.path.dirname(os.path.realpath(__file__))
		other_file_path = os.path.join(current_file_dir, "LGBTQ_Conf.json")
    
		with open(other_file_path, 'r') as read_file:
			
			conf = json.load(read_file)

			_microsoft_subscription_key = conf["microsoft_subscription_key"]
			_microsoft_api_url = conf["microsoft_api_url"]
			
			_pixel_lab_api_url = conf["pixel_lab_api_url"]
			_pixel_lab_api_token = conf["pixel_lab_api_token"]
			_seconds_in_between_unanalyzed_messages = conf["seconds_in_between_unanalyzed_messages"]
			_page_size = conf["page_size"]
            
	except Exception as e:
		print("Unable to load configuration file")
		print(e)
		sys.exit()

def getUnanalyzedDocuments():
	
	global _pixel_lab_api_url, _pixel_lab_api_token, _page_size

	try:
		function_call = "messages/unanalyzed?token=" + _pixel_lab_api_token + "&pageSize=" + str(_page_size)
		api_url = _pixel_lab_api_url + function_call
		printDebug("URL: " + api_url)
		response = requests.get(api_url)
		response = response.json()
		printDebug(response)
	except Exception as e:
		print("Error:")
		print(e)
		return -1

	response_documents = "["

	for doc in response:

		this_response_document = ""

		this_document_id = doc["id"]
		printDebug("document_id: " + str(this_document_id))
		
		request_document = createSimpleDocument(this_document_id, doc["text"])
		printDebug("request_document")
		printDebug(request_document)

		key_phrases = getKeyPhrases(request_document)
		printDebug("key_phrases")
		printDebug(key_phrases)
		
		sentiment_array = getSentimentOnAllTextElements(doc, key_phrases)
		this_document_sentiment = round(sentiment_array[0][1],4)

		this_document_keyword_sentiment = getAvgKeywordSentiment(sentiment_array, len(key_phrases[0][1]))
		
		this_document_size = getTotalCharactersInArray(doc["phrases"])
		
		#formating this document

		this_response_document = initThisDocument(this_document_id, this_document_sentiment, 
			this_document_keyword_sentiment, this_document_size)

		color_gradients = getColorGradients(this_document_sentiment,this_document_keyword_sentiment)
		sentiment_plane = getSentimentPlane(doc["phrases"], sentiment_array )
		printDebug("SENTIMENT PLANE")
		printDebug(sentiment_plane)

		key_phrase_plane = getKeyPhrasePlane(key_phrases)
		printDebug("KEY PHRASES PLANE")
		printDebug(key_phrase_plane)
		
		#text = "The Great Depression began in 1929. By 1933, the GDP in America fell by 25%."
		#entities = getEntities(createSimpleDocument(1, request_document))
		
		#entities = getEntities(request_document)
		#printDebug(entities)
		#entity_plane = getEntityPlane(entities)
		entity_plane = '"entity_plane" : { "num_entities" : 0 }'
		printDebug("ENTITY PLANE")
		printDebug(entity_plane)

		this_response_document = this_response_document + color_gradients
		this_response_document = this_response_document + sentiment_plane
		this_response_document = this_response_document + key_phrase_plane
		this_response_document = this_response_document + entity_plane
		this_response_document = this_response_document + "},"

		response_documents = response_documents + this_response_document
	
	#remove the last comma
	if(response_documents[-1] == ','):	
		response_documents = response_documents[0:-1]
	#close document
	response_documents = response_documents + "]"
	printDebug("Final document")
	printDebug(response_documents)
	return json.loads(response_documents)

def postAnalyzedDocuments(documents):
	
	global _pixel_lab_api_url, _pixel_lab_api_token

	try:
		function_call = "messages/analyzed?token=" + _pixel_lab_api_token
		post_url = _pixel_lab_api_url + function_call
		print("post url: " + post_url)
	 
		headers = {'Content-Type': 'application/json'}
		
		response = requests.post(
            post_url, headers=headers, json=documents)
		
		print(response.status_code)
	except Exception as e: 
		print(e)
		print("Unable to post analyzed documents")

def createSimpleDocument(id_num, text):

	document = {'documents' : [{'id': id_num, 'text': text}]}
	return document

def getAvgKeywordSentiment(sentiment_array, num_key_phrases):
	
	avg_keyword_sentiment = 0
	response = 0
	#print("num_key_phrases: " + str(num_key_phrases) )
	for x in range(1, num_key_phrases + 1):
		#print(str(sentiment_array[x]))
		avg_keyword_sentiment = avg_keyword_sentiment + sentiment_array[x][1]
	try:
		response = round(avg_keyword_sentiment/num_key_phrases,4)
		return response
	except:
		# returning -1 means num_key_phrases was equal to zero
		return -1 

def getSentimentOnAllTextElements(doc, key_phrases):
	
	counter = 1;

	temp_doc = '{"documents" : ['
	temp_doc = temp_doc + '{"id": "' + str(counter) + '", "text": "' + doc['text']  + '"},'
	counter+=1
	#adding key phrases
	for kp in key_phrases[0][1]:
		temp_doc = temp_doc + '{"id": "' + str(counter) + '", "text": "' + kp + '"},'
		counter+=1
	#adding sentences
	for s in doc["phrases"]:
		temp_doc = temp_doc + '{"id": "' + str(counter) + '", "text": "' + s + '"},'
		counter+=1
	#removing the last comma
	temp_doc = temp_doc[0:-1]

	#closing the document
	temp_doc = temp_doc + ']}'
	_json_doc = json.loads( temp_doc )

	return getSentiment(_json_doc)

def initThisDocument(document_id, sentiment, avg_keyword_sentiment, document_size):
	this_document = "{"
	this_document = this_document + '"document_id" : "' + str(document_id) + '",'
	this_document = this_document + '"sentiment" : ' + str(sentiment) + ','
	this_document = this_document + '"avg_keyword_sentiment" : ' + str(avg_keyword_sentiment) + ','
	this_document = this_document + '"document_size" : ' + str(document_size) + ','

	return this_document

def getColorGradients(sentiment, avg_keyword_sentiment):
	
	s_rgb = getRGB(sentiment)
	k_rgb = getRGB(avg_keyword_sentiment)

	color_gradients = '"color_gradients" : {'
	color_gradients = color_gradients +'"base_color" : [' + str(s_rgb[0]) + ',' + str(s_rgb[1]) + ',' + str(s_rgb[2]) + '],' 
	color_gradients = color_gradients +'"keyword_color" : [' + str(k_rgb[0]) + ',' + str(k_rgb[1]) + ',' + str(k_rgb[2]) + ']' 
	color_gradients = color_gradients + '},'
	
	return color_gradients

def getSentimentPlane(sentence_array, sentiment_array):
	
	num_sentences = len(sentence_array)
	sentence_sentiment = []
	total_characters = getTotalCharactersInArray(sentence_array)
		
	for x in range(len(sentiment_array) - num_sentences, len(sentiment_array)):
		sentence_sentiment.append(sentiment_array[x][1])

	#calculating sentiment plane
	sentiment_plane = '"sentiment_plane" : { "num_subsentence" : ' + str(num_sentences) + ', "weights" : ['
	for x in range(0, len(sentence_array)):
		sentiment_plane = sentiment_plane + '{"percentage" : ' + str(round(100*(len(sentence_array[x])/total_characters))) + ',' 
		sentiment_plane = sentiment_plane +  '"sentiment" : ' + str(round(sentence_sentiment[x],4)) + '},' 
	
	#remove the last comma
	if(sentiment_plane[-1] == ','):	
		sentiment_plane = sentiment_plane[0:-1]
	
	sentiment_plane = sentiment_plane + ']},'
	
	return sentiment_plane

def getKeyPhrasePlane(key_phrase):
	
	key_phrases_array = key_phrase[0][1]
	num_key_phrases = len(key_phrases_array)
	num_char_in_key_phrase = getTotalCharactersInArray(key_phrases_array)

	#calculating key phrase plane
	key_phrase_plane = '"key_phrase_plane" : { "num_key_phrases" : ' + str(num_key_phrases) + ', "weights" : ['
	for kp in key_phrases_array:
		key_phrase_plane = key_phrase_plane + '{"phrase" : "' + kp + '",' 
		key_phrase_plane = key_phrase_plane +  '"percentage" : ' + str( round( 100*len(kp)/num_char_in_key_phrase)) + '},' 
	
	#remove the last comma
	if(key_phrase_plane[-1] == ','):
		key_phrase_plane = key_phrase_plane[0:-1]	
	
	key_phrase_plane = key_phrase_plane + ']},'
	
	return key_phrase_plane

def getEntityPlane(entities):
	if(len(entities) > 0):
		num_entities = int(len(entities[0][1])*0.5)
		entity_array = countTypesInArray(entities[0][1])

		entity_plane = '"entity_plane" : { "num_entities" : ' + str(num_entities) + ', "weights" : ['
		for e in entity_array:
			
			entity_plane = entity_plane + '{"name" : "' + e[0] + '",' 
			entity_plane = entity_plane +  '"percentage" : ' + str( round( 100*len(e[1])/num_entities)) + '},' 
			
		#remove the last comma
		if(entity_plane[-1] == ','):
			entity_plane = entity_plane[0:-1]
		
		entity_plane = entity_plane + ']}'
		
		return entity_plane
	else: 
		return 'entity_plane : {}'

def countTypesInArray(array):
	return_array = [] 
	
	while len(array)>0:
		
		index = []
		temp_array = []

		subtype = array[1]
		#print("Adding subtype to array: " + subtype)
		temp_array.append(subtype) #the first type/subtype in array
		
		#moving through the array looking for types only
		for x in my_range(1, len(array)-1, 2):
			#print("comparing: " + array[x] + " with " + subtype)
			if(array[x] == subtype):
				#print("they are the same!, store index #" + str(x))
				index.append(x)
		
		temp_array.append(index)
		return_array.append(temp_array)
		counter = 0
		for i in index:
			#print("index: " + str(i))
			#print("deleting: " + array[i -1 ])
			del array[(i - 1) - 2*counter] #delete the name before the type
			#print("deleting: " + array[i -1 ])
			del array[(i - 1) - 2*counter] #delete the type
			counter+=1

	return return_array

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step


def getTotalCharactersInArray(array):
	accum = 0;
	for a in array:
		accum = accum + len(a)
	return accum

def getSentiment(documents):
	global _microsoft_api_url, _microsoft_subscription_key
	
	return_array = []

	sentiment_api_url = _microsoft_api_url + "sentiment"
	headers   = {"Ocp-Apim-Subscription-Key": _microsoft_subscription_key}
	response  = requests.post(sentiment_api_url, headers=headers, json=documents)
	sentiment = response.json()
	if _debug:
		print("Sentiment response")
		print(sentiment)
	for d in sentiment["documents"]:
		temp = []
		temp.append(d["id"])
		temp.append(d["score"])
		return_array.append(temp)

	return return_array

def getKeyPhrases(documents):
	global _microsoft_api_url, _microsoft_subscription_key
	
	return_array = []

	key_phrase_api_url = _microsoft_api_url + "keyPhrases"
	headers   = {'Ocp-Apim-Subscription-Key': _microsoft_subscription_key}
	response  = requests.post(key_phrase_api_url, headers=headers, json=documents)
	phrases = response.json()
	if _debug:
		print("keyPhrases response")
		print(phrases)

	for d in phrases["documents"]:
		temp = []
		temp.append(d["id"])
		temp.append(d["keyPhrases"])
		return_array.append(temp)

	return return_array

def getEntities(documents):
	global _microsoft_api_url, _microsoft_subscription_key
	try:
		return_array = []

		entity_url = _microsoft_api_url + "entities"
		headers   = {"Ocp-Apim-Subscription-Key": _microsoft_subscription_key}
		response  = requests.post(entity_url, headers=headers, json=documents)
		entities = response.json()

		for d in entities["documents"]:
			temp_d = []
			temp_e = []
			
			temp_d.append(d["id"])
			for e in d["entities"]:
				
				temp_e.append(e["name"])
			
				try:
					subtype = e["subType"]
				except:
					subtype = "none"
				
				temp_e.append(e["type"] + "/" + subtype)

			temp_d.append(temp_e)	
			return_array.append(temp_d)

		return return_array
	except Exception as e:
		return []

def getLanguage(documents):
	global _microsoft_api_url, _microsoft_subscription_key

	return_array = []

	language_api_url = _microsoft_api_url + "languages"

	headers   = {"Ocp-Apim-Subscription-Key": _microsoft_subscription_key}
	response  = requests.post(language_api_url, headers=headers, json=documents)
	languages = response.json()
	if _debug:
		print("Languages response")
		print(languages)
	
	for d in languages["documents"]:
		temp = []
		temp.append(d["id"])
		
		for l in d["detectedLanguages"]:
			temp2 = []
			temp2.append(l["name"])
			temp2.append(l["score"])
			temp.append(temp2)
		
		return_array.append(temp)

	return return_array

def printSentimentArray(sentiment_array):
	for i in sentiment_array:
		print("id: " + str(i[0]) + ", score: " + str(i[1]) ) 

def printKeyPhrases(key_phrases_array):
	for i in key_phrases_array:
		s = "id: " + str(i[0]) + ", key phrases: ["
		for kp in i[1]:
			s = s + "'" + kp + "',"
		s = s[0:-1]
		s = s+"]"
		print(s)

def printLanguageArray(language_array):
	for i in language_array:
		s = "id: " + str(i[0]) + ", languages: ["
		s = s + "[name: '" + str(i[1][0]) + "', score: " + str(i[1][1]) + "],"
		s = s[0:-1]
		s = s+"]"
		print(s)


def getRGB(sentiment):

	r = 0
	g = 0
	b = 0

	if(sentiment < 0.33):
		t = scale(sentiment, 0, 0.33, 0, 0.5*math.pi)
		sin_t = math.sin(t)
		cos_t = math.cos(t)

		r = round(scale(cos_t, 0, 1, 0, 255))
		g = 0
		b = round(scale(sin_t, 0, 1, 0, 255))
	
	elif(sentiment < 0.66):
		t = scale(sentiment, 0.33, 0.66, 0, 0.5*math.pi)
		sin_t = math.sin(t)
		cos_t = math.cos(t)

		r = 0
		g = round(scale(sin_t, 0, 1, 0, 255))
		b = round(scale(cos_t, 0, 1, 0, 255))

	else:
		t = scale(sentiment, 0.66, 1, 0, 0.5*math.pi)
		sin_t = math.sin(t)
		cos_t = math.cos(t)

		r = round(scale(sin_t, 0, 1, 0, 255))
		g = round(scale(cos_t, 0, 1, 0, 255))
		b = 0

	return [r, g, b]
	#elif(sentiment < 0.66):

def scale(value, rmin, rmax, tmin, tmax):

	return ( ((value - rmin)/(rmax - rmin)) * (tmax - tmin) ) + tmin

def printDebug(msg):
	if(_debug):
		print(msg)

loadConfiguration()

while True:
	try:
		print("Fetching " + str(_page_size) + " unanalyzed messages")
		response_documents = getUnanalyzedDocuments()
		printDebug("Response Documents")
		printDebug(response_documents)

		if(response_documents != -1):
			#print(response_documents)
			if len(response_documents) > 0:
				print("Uploading analyzed messages to Pixel Lab's API")
				print("")
				postAnalyzedDocuments(response_documents)
			else:
				print("No new messages awaiting analysis")
				print("")
		time.sleep(_seconds_in_between_unanalyzed_messages)
	except Exception as e: 
		print("Error")
		print(e)
		time.sleep(1)

